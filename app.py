"""
Author      : Disha Agrawal
Created At  : 27 June 2019
Description : This is flask application with a get API to get the company details from company id
Dependancies: 1. Data file "CompaniesList.json" which contain company details.
              2. Module name "Company.py" to process name and generate logo
Assumption  : The special characters and space will not be considered in the logo
"""

from flask import Flask, request, jsonify,render_template
import json
import os
import Company

app = Flask(__name__)
app.config["DEBUG"] = True
 
@app.errorhandler(404)
def page_not_found(e):
    return jsonify({ "status": "404","data" : "Page Not Found!" })


@app.route('/', methods=['GET'])
def renderHomePage():
    '''
    Function to renders home page
    '''
    return render_template('companyHome.html', companyLogo = " ")

@app.route('/api/v1/resources/company', methods=['GET'])
def api_id():
    """
        Check if an ID was provided as part of the URL.
        If ID is provided, process it and return the suitable output.
        If no ID is provided, display an error in the browser.
    """
    if 'id' in request.args:
        companyIdByUser = (request.args['id'])
        result = Company.getCompanyDetails(companyIdByUser) 
        print(result)
        return render_template('companyHome.html', companyLogo = result)
    else:
        result = {"status":"201", "data":"id not present in the argument"}
        print(result)
        return render_template('companyHome.html', companyLogo = result)
        
if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8888)
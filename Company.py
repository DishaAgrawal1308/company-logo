"""
Author      : Disha Agrawal
Created At  : 29 June 2019
Description : This is python module which contains function to read data from file and process the company name to get the logo characters
Dependancies: 1. Data file "CompaniesList.json" which contain company details.
Assumption  : The special characters and space will not be considered in the logo
"""

import json
import os
from collections import Counter 
from operator import itemgetter
#data_source is the global variable to store the location of json file
data_source = os.path.join(os.path.dirname(os.path.realpath(__file__)), "CompaniesList.json") 

CHARACTER_LIMIT_IN_LOGO =  3 #Constant defines the number of character to be considered while forming the company logo


'''
Author: Disha Agrawal
Modified at: 29 June 2019
Funtion purpose: Read the json data stored in a file
Input:  Path of the file to be read
Output: Json data read from the file
'''
def read_json_data(data_source):
    try:
        fp = open(data_source,'r',encoding="utf8")
        data = json.loads(fp.read())
        return data
    except:
        #Exception in case of failure to read, send an empty array
        data = []
        return data
'''
Author: Disha Agrawal
Modified at: 29 June 2019
Funtion purpose: Find the company details by Id from the data of all the company details
Input:  1. Details of all the company
        2. Id of the company to be searched
Output: Company details
'''
def findCompanyByID(companyData, companyIdByUser):
    matchedCompany = {} #Variable to track the company details
    for company in companyData:
        if company['CompanyId'] == companyIdByUser:
            matchedCompany = company
            break
    return matchedCompany

'''
Author: Disha Agrawal
Modified at: 29 June 2019
Funtion purpose: Function to retrive the company name from the specific company details
Input:  1. Details of the specific company
Output: Name of the company
'''
def getCompanyName(company):
    companyName = company['Company Name'] #"Company Name" is assumed to be the key in the json data read from the file
    return companyName

'''
Author: Disha Agrawal
Modified at: 29 June 2019
Funtion purpose: Provide the company details along with logo characters based on the company id
Input:  1. Comapny id of a specific company
Output: Details of the company
'''
def getCompanyDetails(companyIdByUser):   
    userAskedCompany = { }
    '''
    1. Get the data of all the company
    2. Check if the id provided is found in the data
    3. If the provided Id is valid, get the company name
    4. Get the logo characters based on the company name
    '''
    allCompaniesDetails = read_json_data(data_source) #All comapany details are fetched
    if not allCompaniesDetails:
        result = {
            "status" : "201",
            "data" : "Company Details is not present or could not be retrived!"
        }
        return result

    #check if the provided id is present in the list
    userAskedCompany = findCompanyByID(allCompaniesDetails, companyIdByUser)

    if not userAskedCompany:
        #Return error message in case of invalid id
        result = {
            "status" : "201",
            "data" : "Enter a valid Company ID!"
        }
    else:
        #Get the company name in case of valid company id
        userAskedCompanyName = getCompanyName(userAskedCompany)
        if not userAskedCompanyName:
            #return the error in case of empty company name
            result = { 
            "status" : "201",
            "data" : "Company name is not available"
            }
        else:
            #Get the logo characters in case of valid company name
            userAskedCompanyLogoCharacters = getLogoCharactersForCompanyUsingName(userAskedCompanyName)
            userAskedCompany['logoCharacters'] =  userAskedCompanyLogoCharacters
            result = { 
                "status" : "200",
                "data" : userAskedCompany
            }
    return (result)


'''
Author: Disha Agrawal
Modified at: 29 June 2019
Funtion purpose:  Generates the logo characters based on the company name
Input:  Name of a specific company
Output: Logo characters for the company
'''
def getLogoCharactersForCompanyUsingName(companyName):
    '''
    1. Convert the name to the lower case
    2. Remove all the spaces and special characters
    3. Get the frequency of occurenece of each character in the name
    4. Process the frequency to get the N characters for the logo
    '''
    companyName = companyName.lower()
    companyNameWithoutSpecialCharacters = ''.join(character for character in companyName if character.isalnum())
    frequencyOfCharactersInCompanyName = Counter(companyNameWithoutSpecialCharacters)
    
    companyNameCharacter2DArraySortedByFrequency =   sorted(frequencyOfCharactersInCompanyName.items(), key=itemgetter(1), reverse=True)
    companyLogoCharacters = getNCompanyLogoCharactersBasedOnFrequencyTuple(companyNameCharacter2DArraySortedByFrequency, CHARACTER_LIMIT_IN_LOGO, [])
    return companyLogoCharacters

'''
Author: Disha Agrawal
Modified at: 29 June 2019
Funtion purpose:  Finds N characters for the company logo based on the frequency of occurence of the characters
Input:  Tuple with the list of characters and their occurence freqency
Output: First N logo characters for the company
'''
def getNCompanyLogoCharactersBasedOnFrequencyTuple(companyFrequencyTuple, characterLimit, companyLogoCharacters):
    print("processing logo characters",companyFrequencyTuple , characterLimit, companyLogoCharacters)
    if characterLimit > 0 and companyFrequencyTuple :
        highestFrequency = companyFrequencyTuple[0][1]
        possibleLogoCharacters = []
        possibleLogoCharacters.append(companyFrequencyTuple[0][0])
        companyFrequencyTuple = companyFrequencyTuple[1:]
        for character in companyFrequencyTuple:
            if character[1] == highestFrequency:
                possibleLogoCharacters.append(character[0])
                companyFrequencyTuple = companyFrequencyTuple[1:]
            else:
                break
        sortedPossibleLogoCharacters = sorted(possibleLogoCharacters)
        possibleLogoCharactersCount = min(len(sortedPossibleLogoCharacters), characterLimit)
        for c in range(possibleLogoCharactersCount):
            companyLogoCharacters.append(sortedPossibleLogoCharacters[c])
        characterLimit -= possibleLogoCharactersCount
        companyLogoCharacters = getNCompanyLogoCharactersBasedOnFrequencyTuple(companyFrequencyTuple,characterLimit,companyLogoCharacters)
        return companyLogoCharacters
    else:
        return companyLogoCharacters